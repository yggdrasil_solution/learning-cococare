package model.obj.lib.ext;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.AccessValue;
import cococare.common.CCFieldConfig.Accessible;
import static cococare.common.CCLogic.isNull;
import static cococare.common.CCLogic.isNotNull;
import cococare.common.CCTypeConfig;
import cococare.database.CCEntity;
import cococare.database.CCEntityBo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import model.obj.lib.LibBook;
import model.obj.lib.ext.LibEnum.ReviewStar;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "lib_book_reviews")
@CCTypeConfig(label = "Book Review", uniqueKey = "@book.title")
public class LibBookReview extends CCEntity {

    @Column(length = 6)
    @CCFieldConfig(accessible = Accessible.MANDATORY, sequence = "R000", unique = true, requestFocus = true)
    private String code;
    @CCFieldConfig(accessValue = AccessValue.METHOD, accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "title", optionReflectKey = "bookId")
    transient private LibBook book;
    private Long bookId;
    @Enumerated(EnumType.STRING)
    private ReviewStar star;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LibBook getBook() {
        if (isNull(book) && isNotNull(bookId)) {
            book = CCEntityBo.INSTANCE.getById(LibBook.class, bookId);
        }
        return book;
    }

    public void setBook(LibBook book) {
        this.book = book;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public ReviewStar getStar() {
        return star;
    }

    public void setStar(ReviewStar star) {
        this.star = star;
    }
}