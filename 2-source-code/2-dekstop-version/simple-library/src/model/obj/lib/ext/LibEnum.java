package model.obj.lib.ext;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class LibEnum {

    public enum ReviewStar {

        STAR_5("*****"), STAR_4("****"), STAR_3("***"), STAR_2("**"), STAR_1("*");
        private String string;

        private ReviewStar(String string) {
            this.string = string;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    public static void main(String[] args) {
        System.out.println(ReviewStar.class);
        // --> class model.obj.lib.ext.LibEnum$ReviewStar
    }
}