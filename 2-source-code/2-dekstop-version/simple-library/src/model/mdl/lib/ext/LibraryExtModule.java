package model.mdl.lib.ext;

//<editor-fold defaultstate="collapsed" desc=" import ">
import cococare.database.CCHibernateModule;
import java.util.Arrays;
import java.util.List;
import model.obj.lib.ext.LibBookReview;
//</editor-fold>

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class LibraryExtModule extends CCHibernateModule {

    public static LibraryExtModule INSTANCE = new LibraryExtModule();

//<editor-fold defaultstate="collapsed" desc=" public method ">
    @Override
    protected List<Class> _getAnnotatedClasses() {
        return (List) Arrays.asList(
                LibBookReview.class);
    }
//</editor-fold>
}