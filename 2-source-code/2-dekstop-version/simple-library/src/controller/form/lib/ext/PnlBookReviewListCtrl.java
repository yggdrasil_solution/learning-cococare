package controller.form.lib.ext;

//<editor-fold defaultstate="collapsed" desc=" import ">
import cococare.database.CCEntityBo;
import cococare.framework.swing.controller.form.PnlDefaultListCtrl;
import model.obj.lib.ext.LibBookReview;
//</editor-fold>

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class PnlBookReviewListCtrl extends PnlDefaultListCtrl {

    @Override
    protected Class _getEntity() {
        return LibBookReview.class;
    }

    @Override
    protected <T> T _getSelectedItem() {
        return (T) CCEntityBo.INSTANCE.foreignKeyToForeignEntity(super._getSelectedItem());
    }
}