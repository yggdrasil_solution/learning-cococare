package controller.form.lib;

//<editor-fold defaultstate="collapsed" desc=" import ">
import static cococare.common.CCClass.extract;
import static cococare.common.CCFormat.formatNumber;
import static cococare.common.CCLogic.isNotNull;
import cococare.framework.model.obj.util.UtilFilter.isIdNotInIds;
import cococare.framework.swing.controller.form.PnlDefaultCtrl;
import static cococare.swing.CCSwing.addListener;
import cococare.swing.component.CCBandBox;
import cococare.swing.component.CCDatePicker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JTextField;
import model.bo.lib.LibBorrowingItemBo;
import model.obj.lib.LibBook;
import static model.obj.lib.LibFilter.isBorrowedFalse;
import static model.obj.lib.LibFilter.isSuspendFalse;
//</editor-fold>

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class PnlBorrowingItemCtrl extends PnlDefaultCtrl {

//<editor-fold defaultstate="collapsed" desc=" private object ">
    private LibBorrowingItemBo borrowingItemBo;
    private CCDatePicker dtpdate;
    private CCBandBox bndBook;
    private JTextField txtBorrowingCost;
    private CCDatePicker dtpDateReturn;
    private JTextField txtBorrowingFine;
//</editor-fold>

    @Override
    protected void _initComponent() {
        super._initComponent();
        dtpdate = (CCDatePicker) parameter.get(callerCtrl.toString() + "dtpDate");
    }

    @Override
    protected void _initEditor() {
        super._initEditor();
        bndBook.getTable().setHqlFilters(
                isSuspendFalse,
                isBorrowedFalse,
                new isIdNotInIds() {
            @Override
            public Object getFieldValue() {
                //get borrowed books from screen
                return extract((List) parameter.get(callerCtrl.toString() + childsValue), "book.id");
            }
        });
    }

    @Override
    protected void _initListener() {
        super._initListener();
        addListener(bndBook, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _doUpdateBookInfo();
            }
        });
    }

    private void _doUpdateBookInfo() {
        LibBook book = bndBook.getObject();
        if (isNotNull(book)) {
            txtBorrowingCost.setText(formatNumber(book.getBorrowingCost()));
            dtpDateReturn.setDate(borrowingItemBo.calculateDateReturn(dtpdate.getDate(), book.getBorrowingLimit()));
            txtBorrowingFine.setText(formatNumber(book.getBorrowingFine()));
        }
    }
}