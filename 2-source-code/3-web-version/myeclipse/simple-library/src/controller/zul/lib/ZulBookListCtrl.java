package controller.zul.lib;

import model.obj.lib.LibBook;
import cococare.framework.zk.controller.zul.ZulDefaultListCtrl;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulBookListCtrl extends ZulDefaultListCtrl {

	@Override
	protected Class _getClass() {
		return _getDefaultToCustomClass();
	}

	@Override
	protected Class _getEntity() {
		return LibBook.class;
	}
}