package controller.zul.sample;

import static cococare.common.CCClass.copy;
import model.bo.lib.LibConfigBo;
import cococare.framework.zk.controller.zul.ZulDefaultCtrl;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulBook2Ctrl extends ZulDefaultCtrl {

	private LibConfigBo configBo;

	@Override
	protected ShowMode _getShowMode() {
		return ShowMode.DIALOG_MODE;
	}

	@Override
	protected void _initObjEntity() {
		super._initObjEntity();
		copy(configBo.loadLibConfig(), objEntity);
	}
}