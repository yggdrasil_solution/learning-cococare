package model.mdl.lib;

import cococare.common.CCLanguage;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class LibraryLanguage extends CCLanguage {

	public static String Lib;
	// menu
	public static String Book_Type;
	public static String Author;
	public static String Publisher;
	public static String Book;
	public static String Member;
	public static String Borrowing;
	public static String Returning;
}