package tutorial;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listgroup;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulListComposerListboxCtrl extends GenericAutowireComposer {
	private Listbox listbox;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doPopulate();
	}

	private void _doPopulate() {
		new Listhead().setParent(listbox);
		new Listheader("Column A", null, "100px").setParent(listbox.getListhead());
		new Listheader("Column B", null, "100px").setParent(listbox.getListhead());
		new Listheader("Column C", null, "100px").setParent(listbox.getListhead());
		new Listheader("Column D", null, null).setParent(listbox.getListhead());
		new Listitem().setParent(listbox);
		new Listcell("Row 1 A").setParent(listbox.getLastChild());
		new Listcell("Row 1 B").setParent(listbox.getLastChild());
		new Listcell("Row 1 C").setParent(listbox.getLastChild());
		new Listcell("Row 1 D").setParent(listbox.getLastChild());
		new Listitem().setParent(listbox);
		new Listcell("Row 2 A").setParent(listbox.getLastChild());
		new Listcell("Row 2 B").setParent(listbox.getLastChild());
		new Listcell("Row 2 C").setParent(listbox.getLastChild());
		new Listcell("Row 2 D").setParent(listbox.getLastChild());
		new Listgroup("Group Sample").setParent(listbox);
		new Listitem().setParent(listbox);
		new Listcell("Row 3 A").setParent(listbox.getLastChild());
		new Listcell("Row 3 B").setParent(listbox.getLastChild());
		new Listcell("Row 3 C").setParent(listbox.getLastChild());
		new Listcell("Row 3 D").setParent(listbox.getLastChild());
		new Listitem().setParent(listbox);
		new Listcell("Row 4 A").setParent(listbox.getLastChild());
		new Listcell("Row 4 B").setParent(listbox.getLastChild());
		new Listcell("Row 4 C").setParent(listbox.getLastChild());
		new Listcell("Row 4 D").setParent(listbox.getLastChild());
	}
}