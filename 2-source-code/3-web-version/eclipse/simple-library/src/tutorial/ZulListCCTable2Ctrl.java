package tutorial;

import model.obj.lib.LibBook;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.impl.MeshElement;

import cococare.zk.CCTable;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulListCCTable2Ctrl extends GenericAutowireComposer {
	private MeshElement meshElement;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doPopulate();
	}

	private void _doPopulate() {
		CCTable table = new CCTable(meshElement, "Code", "Title", "Book Type", "Author", "Publisher");
		table.setColumnWidth(100, 100, 100, 100, null);
		for (LibBook book : Dummy.BOOKS) {
			table.addRow(book.getCode(), book.getTitle(), book.getBookType().getName(), book.getAuthor().getName(), book.getPublisher().getName());
		}
	}
}