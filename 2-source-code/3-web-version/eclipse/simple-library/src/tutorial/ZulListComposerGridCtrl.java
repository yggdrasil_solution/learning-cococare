package tutorial;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Group;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulListComposerGridCtrl extends GenericAutowireComposer {
	private Grid grid;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doPopulate();
	}

	private void _doPopulate() {
		new Columns().setParent(grid);
		new Column("Column A", null, "100px").setParent(grid.getColumns());
		new Column("Column B", null, "100px").setParent(grid.getColumns());
		new Column("Column C", null, "100px").setParent(grid.getColumns());
		new Column("Column D", null, null).setParent(grid.getColumns());
		new Rows().setParent(grid);
		new Row().setParent(grid.getRows());
		new Label("Row 1 A").setParent(grid.getRows().getLastChild());
		new Label("Row 1 B").setParent(grid.getRows().getLastChild());
		new Label("Row 1 C").setParent(grid.getRows().getLastChild());
		new Label("Row 1 D").setParent(grid.getRows().getLastChild());
		new Row().setParent(grid.getRows());
		new Label("Row 2 A").setParent(grid.getRows().getLastChild());
		new Label("Row 2 B").setParent(grid.getRows().getLastChild());
		new Label("Row 2 C").setParent(grid.getRows().getLastChild());
		new Label("Row 2 D").setParent(grid.getRows().getLastChild());
		new Group("Group Sample (Span)").setParent(grid.getRows());
		new Row().setParent(grid.getRows());
		Cell cell = new Cell();
		cell.setColspan(2);
		new Label("Row 3 A - Row 3 B").setParent(cell);
		cell.setParent(grid.getRows().getLastChild());
		new Label("Row 3 C").setParent(grid.getRows().getLastChild());
		cell = new Cell();
		cell.setRowspan(2);
		new Label("Row 3 D - Row 4 D").setParent(cell);
		cell.setParent(grid.getRows().getLastChild());
		new Row().setParent(grid.getRows());
		new Label("Row 4 A").setParent(grid.getRows().getLastChild());
		new Label("Row 4 B").setParent(grid.getRows().getLastChild());
		new Label("Row 4 C").setParent(grid.getRows().getLastChild());
	}
}