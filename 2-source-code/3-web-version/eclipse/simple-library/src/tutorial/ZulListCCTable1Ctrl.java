package tutorial;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.impl.MeshElement;

import cococare.zk.CCTable;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulListCCTable1Ctrl extends GenericAutowireComposer {
	private MeshElement meshElement;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doPopulate();
	}

	private void _doPopulate() {
		CCTable table = new CCTable(meshElement, "Column A", "Column B", "Column C", "Column D");
		table.setColumnWidth(100, 100, 100, null);
		table.addRow("Row 1 A", "Row 1 B", "Row 1 C", "Row 1 D");
		table.addRow("Row 2 A", "Row 2 B", "Row 2 C", "Row 2 D");
		if (table.getMeshElement() instanceof Grid) {
			table.addGroup("Group Sample (Span)");
			table.setNextColspans(2, 1, 1);
			table.setNextRowspans(1, 1, 2);
			table.addRow("Row 3 A - Row 3 B", "Row 3 C", "Row 3 D - Row 4 D");
			table.addRow("Row 4 A", "Row 4 B", "Row 4 C");
		} else if (table.getMeshElement() instanceof Listbox) {
			table.addGroup("Group Sample");
			table.addRow("Row 3 A", "Row 3 B", "Row 3 C", "Row 3 D");
			table.addRow("Row 4 A", "Row 4 B", "Row 4 C", "Row 4 D");
		} else if (table.getMeshElement() instanceof Tree) {
			table.addChildRow(1, "3 A", "3 B", "3 C", "3 D");
			table.addChildRow(2, "4 A", "4 B", "4 C", "4 D");
		}
	}
}