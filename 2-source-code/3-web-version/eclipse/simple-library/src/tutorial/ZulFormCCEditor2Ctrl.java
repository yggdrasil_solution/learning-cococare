package tutorial;

import model.obj.lib.LibBook;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;

import cococare.zk.CCEditor;
import cococare.zk.CCMessage;
import cococare.zk.CCZk;

public class ZulFormCCEditor2Ctrl extends GenericAutowireComposer {
	private Button btnSave;
	private Grid pnlGenerator;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doInit(comp);
	}

	private void _doInit(Component container) {
		final CCEditor editor = new CCEditor(container, LibBook.class);
		editor.generateDefaultEditor(pnlGenerator);
		CCZk.addListener(btnSave, new EventListener() {
			public void onEvent(Event event) throws Exception {
				if (editor.isValueValid()) {
					LibBook book = editor.getValueFromEditor();
					CCMessage.showSaved(editor.saveOrUpdate(book));
					editor.newItem();
				}
			}
		});
	}
}