package tutorial;

import model.obj.lib.LibBook;

public class Dummy {
	public static final LibBook[] BOOKS = {//
	new LibBook("Code 01", "Title 01", "Type 01", "Author 01", "Publisher 01"),//
			new LibBook("Code 02", "Title 02", "Type 01", "Author 02", "Publisher 02"),//
			new LibBook("Code 03", "Title 03", "Type 02", "Author 03", "Publisher 03"),//
			new LibBook("Code 04", "Title 04", "Type 02", "Author 04", "Publisher 04") //
	};
}