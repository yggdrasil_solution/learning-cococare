package tutorial;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericAutowireComposer;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.Treecols;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulListComposerTreeCtrl extends GenericAutowireComposer {
	private Tree tree;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		_doPopulate();
	}

	private void _doPopulate() {
		new Treecols().setParent(tree);
		new Treecol("Column A", null, "100px").setParent(tree.getTreecols());
		new Treecol("Column B", null, "100px").setParent(tree.getTreecols());
		new Treecol("Column C", null, "100px").setParent(tree.getTreecols());
		new Treecol("Column D", null, null).setParent(tree.getTreecols());
		new Treechildren().setParent(tree);
		new Treeitem().setParent(tree.getTreechildren());
		new Treerow().setParent(tree.getTreechildren().getLastChild());
		new Treecell("Row 1 A").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 1 B").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 1 C").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 1 D").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treeitem().setParent(tree.getTreechildren());
		new Treerow().setParent(tree.getTreechildren().getLastChild());
		new Treecell("Row 2 A").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 2 B").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 2 C").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treecell("Row 2 D").setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treechildren().setParent(tree.getTreechildren().getLastChild());
		new Treeitem().setParent(tree.getTreechildren().getLastChild().getLastChild());
		new Treerow().setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild());
		new Treecell("3 A").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("3 B").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("3 C").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("3 D").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treechildren().setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild());
		new Treeitem().setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treerow().setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("4 A").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("4 B").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("4 C").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild());
		new Treecell("4 D").setParent(tree.getTreechildren().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild().getLastChild());
	}
}