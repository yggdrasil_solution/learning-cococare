package model.obj.lib;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cococare.common.CCFieldConfig;
import cococare.common.CCFieldConfig.Accessible;
import cococare.common.CCFieldConfig.Type;
import cococare.common.CCTypeConfig;
import cococare.database.CCEntity;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "lib_books")
@CCTypeConfig(label = "Book", uniqueKey = "title")
public class LibBook extends CCEntity {

	@Column(length = 6)
	@CCFieldConfig(accessible = Accessible.MANDATORY, sequence = "B000", unique = true, requestFocus = true)
	private String code;
	@Column(length = 32)
	@CCFieldConfig(accessible = Accessible.MANDATORY)
	private String title;
	@Column(length = 255)
	@CCFieldConfig(visible = false)
	private String remarks;
	@ManyToOne
	@CCFieldConfig(componentId = "bndBookType", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private LibBookType bookType;
	@ManyToOne
	@CCFieldConfig(componentId = "bndAuthor", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private LibAuthor author;
	@ManyToOne
	@CCFieldConfig(componentId = "bndPublisher", accessible = Accessible.MANDATORY, maxLength = 32, uniqueKey = "name")
	private LibPublisher publisher;
	@CCFieldConfig(label = "Cost", tooltiptext = "Borrowing Cost", accessible = Accessible.MANDATORY, type = Type.NUMBER_FORMAT, visible = false)
	private Double borrowingCost;
	@CCFieldConfig(label = "Limit", tooltiptext = "Borrowing Limit", accessible = Accessible.MANDATORY, type = Type.NUMERIC, maxLength = 2, visible = false)
	private Integer borrowingLimit;
	@CCFieldConfig(label = "Fine", tooltiptext = "Borrowing Fine", accessible = Accessible.MANDATORY, type = Type.NUMBER_FORMAT, visible = false)
	private Double borrowingFine;
	@CCFieldConfig(label = "B", tooltiptext = "Borrowed", componentId = "chkBorrowed", maxLength = 4)
	private Boolean borrowed = false;
	@CCFieldConfig(label = "S", tooltiptext = "Suspend", componentId = "chkSuspend", maxLength = 4)
	private Boolean suspend = false;

	public LibBook() {
	}

	public LibBook(String code, String title, String bookType, String author, String publisher) {
		this.code = code;
		this.title = title;
		this.bookType = new LibBookType();
		this.bookType.setName(bookType);
		this.author = new LibAuthor();
		this.author.setName(author);
		this.publisher = new LibPublisher();
		this.publisher.setName(publisher);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LibBookType getBookType() {
		return bookType;
	}

	public void setBookType(LibBookType bookType) {
		this.bookType = bookType;
	}

	public LibAuthor getAuthor() {
		return author;
	}

	public void setAuthor(LibAuthor author) {
		this.author = author;
	}

	public LibPublisher getPublisher() {
		return publisher;
	}

	public void setPublisher(LibPublisher publisher) {
		this.publisher = publisher;
	}

	public Double getBorrowingCost() {
		return borrowingCost;
	}

	public void setBorrowingCost(Double borrowingCost) {
		this.borrowingCost = borrowingCost;
	}

	public Integer getBorrowingLimit() {
		return borrowingLimit;
	}

	public void setBorrowingLimit(Integer borrowingLimit) {
		this.borrowingLimit = borrowingLimit;
	}

	public Double getBorrowingFine() {
		return borrowingFine;
	}

	public void setBorrowingFine(Double borrowingFine) {
		this.borrowingFine = borrowingFine;
	}

	public Boolean getBorrowed() {
		return borrowed;
	}

	public void setBorrowed(Boolean borrowed) {
		this.borrowed = borrowed;
	}

	public Boolean getSuspend() {
		return suspend;
	}

	public void setSuspend(Boolean suspend) {
		this.suspend = suspend;
	}
}