package model.mdl.lib;

import java.util.Arrays;
import java.util.List;

import model.obj.lib.LibAuthor;
import model.obj.lib.LibBook;
import model.obj.lib.LibBookType;
import model.obj.lib.LibBorrowing;
import model.obj.lib.LibBorrowingItem;
import model.obj.lib.LibMember;
import model.obj.lib.LibPublisher;
import model.obj.lib.LibReturning;
import model.obj.lib.LibReturningItem;
import cococare.database.CCHibernateModule;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class LibraryModule extends CCHibernateModule {

	public static LibraryModule INSTANCE = new LibraryModule();

	@Override
	protected List<Class> _getAnnotatedClasses() {
		return (List) Arrays.asList(
		// parameter
				LibBookType.class, LibAuthor.class, LibPublisher.class,
				// archive
				LibBook.class, LibMember.class,
				// transaction
				LibBorrowing.class, LibBorrowingItem.class, LibReturning.class, LibReturningItem.class);
	}
}