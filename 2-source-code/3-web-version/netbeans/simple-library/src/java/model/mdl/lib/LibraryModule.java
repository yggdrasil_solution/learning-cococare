package model.mdl.lib;

import cococare.database.CCHibernateModule;
import java.util.Arrays;
import java.util.List;
import model.obj.lib.*;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class LibraryModule extends CCHibernateModule {

    public static LibraryModule INSTANCE = new LibraryModule();

    @Override
    protected List<Class> _getAnnotatedClasses() {
        return (List) Arrays.asList(
                //parameter
                LibBookType.class,
                LibAuthor.class,
                LibPublisher.class,
                //archive
                LibBook.class,
                LibMember.class,
                //transaction
                LibBorrowing.class,
                LibBorrowingItem.class,
                LibReturning.class,
                LibReturningItem.class);
    }
}