package model.mdl.lib;

import cococare.framework.zk.CFSetup;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Setup {

    public static void main(String[] args) {
        CFSetup.executeWebRootFile("web");
    }
}