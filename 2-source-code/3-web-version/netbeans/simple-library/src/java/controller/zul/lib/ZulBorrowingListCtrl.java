package controller.zul.lib;

import cococare.framework.zk.controller.zul.ZulDefaultListCtrl;
import model.obj.lib.LibBorrowing;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulBorrowingListCtrl extends ZulDefaultListCtrl {

    @Override
    protected Class _getClass() {
        return _getDefaultToCustomClass();
    }

    @Override
    protected Class _getEntity() {
        return LibBorrowing.class;
    }
}