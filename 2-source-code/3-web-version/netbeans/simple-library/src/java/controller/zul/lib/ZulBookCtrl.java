package controller.zul.lib;

import static cococare.common.CCClass.copy;
import cococare.framework.zk.controller.zul.ZulDefaultCtrl;
import model.bo.lib.LibConfigBo;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulBookCtrl extends ZulDefaultCtrl {

    private LibConfigBo configBo;

    @Override
    protected void _initObjEntity() {
        super._initObjEntity();
        copy(configBo.loadLibConfig(), objEntity);
    }
}