package controller.zul.lib;

import cococare.framework.zk.controller.zul.ZulDefaultListCtrl;
import model.obj.lib.LibMember;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class ZulMemberListCtrl extends ZulDefaultListCtrl {

    @Override
    protected Class _getEntity() {
        return LibMember.class;
    }
}